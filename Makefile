NAME=nibbler
LIB1=libncurses.dll
LIB2=libsfml2d.dll
LIB3=libglfw.dll
LIBA=libaudio.dll
CORE=core
HEADER=includes/
FLAGS=-Wall -Wextra -Werror -g -std=c++11 -Ofast 
DIRSRC=srcs/
DIROBJ=objs/
DIRLIB=lib/
DIRBIN=bin/
INCLUDES=includes/
PWD=$(shell pwd)
CC=clang++

# =====  CORE POGRAM ===== #
DIRCORE=core/
SRCSCORE=$(addprefix $(DIRCORE), \
	main.cpp \
	DisplayerFactory.class.cpp \
	GameManager.class.cpp \
	Entity.class.cpp \
	Snake.class.cpp \
	SnakeEntity.class.cpp \
	Food.class.cpp \
	SpecialFood.class.cpp \
	ChronoTrigger.class.cpp \
	SocketManager.class.cpp \
	ServerManager.class.cpp \
	ClientManager.class.cpp)
OBJSCORE=$(SRCSCORE:%.cpp=$(DIROBJ)%.o)
# ====================== #


# =====  NCURSE DLL ===== #
DIRLIB1=libncurses/
SRCSLIB1=$(addprefix $(DIRLIB1), \
	DisplayerNcurses.class.cpp)
OBJSLIB1=$(SRCSLIB1:%.cpp=$(DIROBJ)%.o)
# ======================= #


# =====  AUDIO DLL ===== #
DIRAUDIO=libaudio/
SRCSAUDIO=$(addprefix $(DIRAUDIO), \
	AudioManager.class.cpp)
OBJSAUDIO=$(SRCSAUDIO:%.cpp=$(DIROBJ)%.o)
# ====================== #


# =====  SFML DLL  ===== #
DIRSFML=SFML
SFML_ARCHIVE=SFML-2.2-osx-clang-universal
DIRLIB2=libsfml/
SRCSLIB2=$(addprefix $(DIRLIB2), \
	DisplayerSfml2D.class.cpp)
OBJSLIB2=$(SRCSLIB2:%.cpp=$(DIROBJ)%.o)
SFMLCC=clang++
SFMLFRAMEWORK= -F$(PWD)/SFML/Frameworks -framework SFML
SFMLLIBS = -L$(PWD)/SFML/lib -lsfml-graphics -lsfml-window -lsfml-system -lsfml-audio
SFML_INCLUDES = $(PWD)/SFML/include
# ====================== #


# =====  GLFW DLL  ===== #
DIRGLFW = glfw-3.1
GLFWINCLUDES = $(DIRGLFW)/include
GLFWFRAMEWORK = -L$(DIRGLFW)/src -lglfw3 -framework Cocoa -framework OpenGL -framework IOKit -framework CoreVideo
DIRLIB3=libglfw/
SRCSLIB3=$(addprefix $(DIRLIB3), \
	DisplayerGlfw.class.cpp)
OBJSLIB3=$(SRCSLIB3:%.cpp=$(DIROBJ)%.o)
DIRGLM= glm
GLMINCLUDES= -I glm/glm
DIRJPEG=libJpg
LIBJPEGINCLUDES= libJpg/include
LIBJPEG= libJpg/lib/libjpeg.a
# ====================== #

all: init $(DIRBIN) $(NAME) \
	$(DIRLIB)$(LIB2) \
	$(DIRLIB)$(LIB1) \
	$(DIRLIB)$(LIB3) \
	$(DIRLIB)$(LIBA)

init: $(DIRSFML) $(DIRJPEG) $(DIRGLFW) $(DIRGLM) $(DIRLIB) \
	$(DIROBJ)$(DIRCORE) \
	$(DIROBJ)$(DIRLIB1) \
	$(DIROBJ)$(DIRLIB2) \
	$(DIROBJ)$(DIRLIB3) \
	$(DIROBJ)$(DIRAUDIO)

$(DIRSFML):
	@echo "Loading \033[32mSFML\033[0m"
	@curl -L -O -# --progress-bar  http://www.sfml-dev.org/files/$(SFML_ARCHIVE).tar.gz
	@tar -xf $(SFML_ARCHIVE).tar.gz
	@mv $(SFML_ARCHIVE) $(DIRSFML)
	@cd $(DIRSFML)/Frameworks/ && ln -s ../extlibs/freetype.framework
	@cd $(DIRSFML)/Frameworks/ && ln -s ../extlibs/sndfile.framework
	@rm $(SFML_ARCHIVE).tar.gz
	@cd includes && ln -s ../SFML/include/SFML SFML
	@echo "$(DIRSFML) \033[33m Ready \033[0m"

$(DIRGLFW):
	@echo "Loading \033[32mGLFW\033[0m"
	@curl -L -O -# --progress-bar http://softlayer-ams.dl.sourceforge.net/project/glfw/glfw/3.1/glfw-3.1.zip
	@unzip -q glfw-3.1.zip
	@rm glfw-3.1.zip
	@cd $(DIRGLFW) && cmake . && make
	@echo "GLFW \033[33m Ready \033[0m"

$(DIRGLM):
	@echo "Loading \033[32mGLM\033[0m"
	@curl -L -O -# --progress-bar http://sourceforge.net/projects/ogl-math/files/glm-0.9.6.3/glm-0.9.6.3.zip/download
	@unzip -q download
	@rm download
	@cd $(DIRGLM) && cmake . && make
	@echo "GLM \033[33m Ready \033[0m"

$(DIRJPEG):
	@echo "Loading \033[32mLIBJPEG\033[0m"
	@curl -L -O -# --progress-bar http://www.ijg.org/files/jpegsrc.v9a.tar.gz
	@tar -xvf jpegsrc.v9a.tar.gz
	@mkdir -p libJpg
	@cd jpeg-9a && ./configure --prefix=$(PWD)/libJpg && make && make install
	@rm -r jpeg-9a jpegsrc.v9a.tar.gz
	@echo "LIBJPEG \033[33m Ready \033[0m"

$(DIRBIN):
	@mkdir -p $(DIRBIN)

$(DIRLIB):
	@mkdir -p $(DIRLIB)

$(DIROBJ)$(DIRCORE):
	@mkdir -p $(DIROBJ)$(DIRCORE)

$(DIROBJ)$(DIRLIB1):
	@mkdir -p $(DIROBJ)$(DIRLIB1)

$(DIROBJ)$(DIRLIB2):
	@mkdir -p $(DIROBJ)$(DIRLIB2)

$(DIROBJ)$(DIRLIB3):
	@mkdir -p $(DIROBJ)$(DIRLIB3)

$(DIROBJ)$(DIRAUDIO):
	@mkdir -p $(DIROBJ)$(DIRAUDIO)

end:
	@echo "\033[2K\t\033[1;36m$(NAME)\t\t\033[0;32m[ready]\033[0m"

$(NAME): $(OBJSCORE)
	@echo "\033[32m===>\033[0m Compiling \033[33m$@\033[0m"
	$(CC) -o $(NAME) $(FLAGS) $^
	@echo "Compilation \033[33m$@ \033[32mSuccess\033[0m"
	@echo ""

$(DIRLIB)$(LIB1): $(OBJSLIB1)
	@echo "\033[32m===>\033[0m Compiling \033[33m$@\033[0m"
	$(CC) -o $@ $(FLAGS) $^ --shared -lncurses
	@echo "Compilation \033[33m$@ \033[32mSuccess\033[0m"
	@echo ""

$(DIRLIB)$(LIB2): $(OBJSLIB2)
	@echo "\033[32m===>\033[0m Compiling \033[33m$@\033[0m"
	$(SFMLCC) -o $@ $(FLAGS) $^ -I $(INCLUDES) -I $(SFML_INCLUDES) $(SFMLFRAMEWORK) $(SFMLLIBS) -dynamiclib -rpath $(PWD)/$(DIRSFML)/lib -rpath $(PWD)/$(DIRSFML)/Frameworks
	@echo "Compilation \033[33m$@ \033[32mSuccess\033[0m"
	@echo ""

$(DIRLIB)$(LIB3): $(OBJSLIB3)
	@echo "\033[32m===>\033[0m Compiling \033[33m$@\033[0m"
	$(CC) -o $@ $(FLAGS) $^ -I $(INCLUDES) -I $(GLFWINCLUDES) $(LIBJPEG) $(GLFWFRAMEWORK) -dynamiclib -rpath $(PWD)/libJpg/lib
	@echo "Compilation \033[33m$@ \033[32mSuccess\033[0m"
	@echo ""

$(DIRLIB)$(LIBA): $(OBJSAUDIO)
	@echo "\033[32m===>\033[0m Compiling \033[33m$@\033[0m"
	$(SFMLCC) -o $@ $(FLAGS) $^ -I $(INCLUDES) -I $(SFML_INCLUDES) $(SFMLFRAMEWORK) $(SFMLLIBS) -dynamiclib -rpath $(PWD)/$(DIRSFML)/lib -rpath $(PWD)/$(DIRSFML)/Frameworks
	@echo "Compilation \033[33m$@ \033[32mSuccess\033[0m"
	@echo ""

$(DIROBJ)%.o: $(DIRSRC)%.cpp $(INCLUDES)
	@echo "\033[32m->\033[0m Compiling \033[33m$<\033[0m"
	$(CC) -o $@ $(FLAGS) -c $< -I$(INCLUDES) -I $(SFML_INCLUDES) -I $(GLFWINCLUDES) $(GLMINCLUDES) -I $(LIBJPEGINCLUDES)
	@echo ""

$(DIROBJ)%.o: $(DIRSRC)%.cpp
	@echo "\033[32m->\033[0m Compiling \033[33m$<\033[0m"
	$(CC) -o $@ $(FLAGS) -c $< -I$(INCLUDES) -I $(SFML_INCLUDES) -I $(GLFWINCLUDES) $(GLMINCLUDES) -I $(LIBJPEGINCLUDES)
	@echo ""

clean: clear
	rm -rf $(DIROBJ)

clear:
	rm -f `find . -name "*~"`

fclear: fclean clear
	rm -rf $(DIRJPEG)
	rm -rf $(DIRGLFW)
	rm -rf $(DIRSFML)
	rm -rf $(DIRGLM)

fclean: clean
	rm -rf $(DIRBIN)
	rm -rf $(DIRLIB)
	rm -rf $(DIROBJ)
	rm -f $(NAME)

re: fclean all

gdb:
	make
	reset
	gdb $(NAME)

#depend:
#	makedepend  -- $(FLAGS) -I$(INCLUDES) -- $(addprefix $(DIRSRC), $(SRCSCORE))
