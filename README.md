# Nibbler #

### Network snake game, stand alone client and server in one program ###

Use c++ interface to switch shared displayer class:

* 1 ncurses
* 2 SFML2 2D graphics
* 3 GLFW - opengl 3D

### Setup ###

* on osx and linux:
```
make
```

All libs are downloaded and compiled with makefile, not installed simply remove your folder if you want to delete.

GLFW not available at this adress anymore. need to edit makefile.
Better use git submodule.

### Usage ###

`./nibbler <width> <height>` for local game.

`./nibbler <width> <height> <port>` for server.

`./nibbler <yourip:port>` for client

Server waits for two clients to connect before launching the game

### picture ###

![Nibbler.jpg](https://bitbucket.org/repo/9jGjgg/images/2072391588-Nibbler.jpg)

### In Game ###

* space to pause, 1, 2, 3 to swith displaying shared library.
* arrows to move

### Who do I talk to? ###

* rbernand@student.42.fr
* caupetit@student.42.fr