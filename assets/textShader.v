# version 330 core

layout(location = 1) in vec2 vertPos;
layout(location = 2) in vec2 textUV;
uniform vec2 screen;

out vec2 UV;

void main(void)
{
	vec2 pos = vertPos - vec2(screen.x  / 2, screen.y / 2);
	pos /= vec2(screen.x / 2, screen.y  / 2);
	gl_Position = vec4(pos, 0, 1);
	UV = textUV;
}
