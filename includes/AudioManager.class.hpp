#ifndef AUDIOMANAGER_CLASS_HPP
# define AUDIOMANAGER_CLASS_HPP

# include <SFML/Audio.hpp>
# include <iostream>
# include "IAudioManager.interface.hpp"

class AudioManager : public IAudioManager
{
	public:
		AudioManager(void);
		~AudioManager(void);

		void	init(void);
		void				popFood ( void ) ;

		class	InitError : public std::exception
		{
		public:
			InitError(void);
			InitError(const std::string error);
			InitError(const InitError & rhs);
			~InitError(void);
			virtual const char *	what(void) const noexcept;
		private:
			InitError & operator=(const InitError & rhs);
			const std::string	_what;
		};

	private:
		AudioManager(AudioManager const & rhs);
		AudioManager & operator=(AudioManager const & rhs);

		sf::Music			_mainTheme;
		sf::Sound			_popfood;
		sf::SoundBuffer		_sBuff;
};

# endif
