#ifndef CHRONOTRIGGER_CLASS_CPP
# define CHRONOTRIGGER_CLASS_CPP

# include <chrono>
# include <unistd.h>
# include "conf.h"

# define CHRONO			std::chrono
# define HIGH_CLOCK		CHRONO::high_resolution_clock
# define STEADY_CLOCK	CHRONO::steady_clock

class ChronoTrigger
{
public:
	ChronoTrigger(void);
	ChronoTrigger(int fps);
	~ChronoTrigger(void);

	void	manageFramerate(void);
	void	setGameBegin(void);
	void	setFpsLimit(float);

	double	getDeltaTime(void) const;
	int		getFramerate(void) const;
	int		getTimeElapsed(void) const;

private:
	typedef	CHRONO::duration<long double, std::micro> fpsTime;

	ChronoTrigger(const ChronoTrigger & src);
	ChronoTrigger & operator=(const ChronoTrigger & rhs);

	void		_setFramerate(STEADY_CLOCK::time_point now);

	STEADY_CLOCK::time_point	_begin;
	STEADY_CLOCK::time_point	_gameBegin;
	STEADY_CLOCK::time_point	_lastFrame;
	STEADY_CLOCK::time_point	_framePoint;
	fpsTime						_fpsLimitTime;
	fpsTime						_frameDuration;
	int							_frameNb;
	int							_framerate;
};

#endif
