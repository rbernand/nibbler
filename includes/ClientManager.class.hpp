// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   ClientManager.class.hpp                            :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: caupetit <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/03/26 15:33:16 by caupetit          #+#    #+#             //
//   Updated: 2015/03/26 20:41:24 by caupetit         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef CLIENTMANAGER_CLASS_HPP
# define CLIENTMANAGER_CLASS_HPP

#include <iostream>
#include <sstream>

# include <sys/socket.h>
# include <netdb.h>
# include <thread>

# include <GameManager.class.hpp>
# include <SocketManager.class.hpp>

class ClientManager : public GameManager, SocketManager
{
	public:
		ClientManager ( std::string str);
		~ClientManager ( void );
		void		launch ( void ) ;
		void		init ( void ) ;

	private:
		e_key					_manageInputs ( void ) ;
		void					_updateDisplay ( void );
		void					_updateEntity ( void );
		void					_loop(void);
		void					_getStatus();
		struct hostent *		_host_info;
		int						_sock;
		struct sockaddr_in		_sin;
};

std::ostream		&operator<<(std::ostream &o, ClientManager const &rhs );

#endif  /* CLIENTMANAGER_CLASS_HPP */
