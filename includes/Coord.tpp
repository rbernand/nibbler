#ifndef COORD_HPP
# define COORD_HPP

template<typename T>
struct Coord
{
	Coord (T x, T y) :
		x(x),
		y(y)
	{}
	Coord ( void ) :
		x(0),
		y(0)
	{}
	Coord & operator=(Coord const & rhs)
	{
		x = rhs.x;
		y = rhs.y;
		return *this;
	}
	bool operator==(Coord const & rhs)
	{
		if (x == rhs.x && y == rhs.y)
			return (true);
		return (false);
	}

	T				x;
	T				y;
};

typedef Coord<int>			coord;
typedef Coord<float>		coordf;

#endif  /* COORD_HPP */
