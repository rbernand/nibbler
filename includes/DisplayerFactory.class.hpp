#ifndef DISPLAYERFACTORY_CLASS_HPP
# define DISPLAYERFACTORY_CLASS_HPP

#include <iostream>
#include <sstream>
#include <dlfcn.h>
#include <exception>
#include <map>

#include <IDisplayer.interface.hpp>
#include <conf.h>

typedef IDisplayer * (*t_create)(void);

class DisplayerFactory
{
	public:
		class InvalidDisplayer : public std::exception
		{
			public:
				InvalidDisplayer ( std::string );
				virtual const char *		what( void ) const noexcept;
			private:
				const std::string			_msg;
		};
		DisplayerFactory ( void );
		~DisplayerFactory (void);

		IDisplayer *		load(e_displayer displayer);

	private:
		DisplayerFactory &	operator=( DisplayerFactory const & rhs );
		DisplayerFactory ( DisplayerFactory const & src );

		static const char *						_path[NB_LIB];
};

#endif  /* DISPLAYERFACTORY_CLASS_HPP */
