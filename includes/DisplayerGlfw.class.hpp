#ifndef DISPLAYERGLFW_CLASS_HPP
# define DISPLAYERGLFW_CLASS_HPP

# define GLFW_INCLUDE_GLCOREARB

# include <GLFW/glfw3.h>
# include <exception>
# include <map>
# include <vector>
# include <iostream>
# include <fstream>
# include <string>
# include <cmath>
# include <algorithm>
# include <sstream>
# include <vector>
# include <glm.hpp>
# include <gtc/matrix_transform.hpp>
# include <gtx/transform.hpp>
# include <jpeglib.h>

# include "IDisplayer.interface.hpp"
# include "Coord.tpp"

# define SQUARE_SIZE	2.5f
# define TEXT_SIZE		320
# define LETTER_SIZE	32

typedef		std::map<int, e_key>	Keys;

class DisplayerGlfw : public IDisplayer
{
public:
	DisplayerGlfw(void);
	~DisplayerGlfw(void);

	void	init(int size_x, int size_y);
	void	close(void);

	void	put_score(const int score);
	void	put_framerate(const int framerate);
	void	put_time(int sec);
	void	put_entity(float x, float y, e_type type);
	e_key	get_input(void) const;
	void	clear_window(void);
	void	draw(void);

	class	InitError : public std::exception
	{
	public:
		InitError(void);
		InitError(const std::string error);
		InitError(const InitError & rhs);
		~InitError(void);
		virtual const char *	what(void) const noexcept;
	private:
		InitError & operator=(const InitError & rhs);
		const std::string	_what;
	};

private:
	DisplayerGlfw(const DisplayerGlfw & rhs);
	DisplayerGlfw & operator=(const DisplayerGlfw & rhs);

	static const std::string	_getFileContent(const std::string & path);
	static void		_checkShaderError(GLuint shader, GLuint flag,
									  bool isProgram, const std::string info);
	static void		_checkGlError(const std::string & where);
	static void		_win_close_callback(GLFWwindow *win);
	static void		_key_callback(GLFWwindow* window, int key,
							  int scancode, int action, int mods);

	void			_setCurrentKey(const e_key key);
	GLuint			_loadShader(const std::string & vertexShaderPath,
								const std::string & fragmentShaderPath);
	void			_loadMesh(void);
	void			_putWalls(void);
	void			_putText(const std::string & text, int x, int y);
	void			_loadImg(void);

	static Keys				_keys;
	static const GLfloat	_vertArr[];
	static const glm::vec3	_colors[4];
	GLuint					_test;
	GLuint					_shaderID;
	GLuint					_textShaderID;
	GLuint					_vBuffer;
	GLFWwindow *			_window;
	coord					_map_size;
	coord					_win_size;
	coord					_glViewSize;
	e_key					_key;
	float					_winRatio;
	GLint					_attr;
	glm::mat4				_projection;
	glm::mat4				_view;
	GLuint					_textID;

};

#endif
