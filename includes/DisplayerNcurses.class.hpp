#ifndef DISPLAYERNCURSES_CLASS_HPP
# define DISPLAYERNCURSES_CLASS_HPP

# include <iostream>
# include <sstream>
# include <curses.h>
# include <map>

# include <IDisplayer.interface.hpp>

# define OFFSET_X			0
# define OFFSET_Y			0

enum			e_color
{
	_WHITE = 100,
	_RED,
	_GREEN,
	_BLUE,
	_RV
};

class DisplayerNcurses : public IDisplayer
{
	public:
		DisplayerNcurses ( void );
		~DisplayerNcurses (void);

		void	init(int size_x, int size_y);
		void	close(void);
		void	put_score(const int score);
		void	put_framerate(const int framerate);
		void	put_entity(float x, float y, e_type type);
		void	put_time(int sec);
		e_key	get_input(void) const;
		void	clear_window(void);
		void	draw(void);

	private:
		DisplayerNcurses &	operator=( DisplayerNcurses const & rhs );
		DisplayerNcurses ( DisplayerNcurses const & src );
		void				display_snake_head ( int x, int y ) const;
		void				display_snake ( int x, int y ) const;
		void				_putShape ( int y, int x, const char * shape, chtype h);

		int 				_size_x;
		int 				_size_y;
		WINDOW *			_main_win;
		WINDOW *			_gui_win;
		static std::map<int, e_key>		_key_map;
		static std::map<int, e_key>					create_map ( void );
};

std::ostream		&operator<<(std::ostream &o, DisplayerNcurses const &rhs );


extern "C" IDisplayer *			create( void );

#endif  /* DISPLAYERNCURSES_CLASS_HPP */
