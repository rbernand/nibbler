#ifndef DISPLAYERSFML2D
# define DISPLAYERSFML2D

# include <SFML/Window.hpp>
# include <SFML/Graphics.hpp>
# include <exception>
# include <sstream>
# include "Coord.tpp"
# include "IDisplayer.interface.hpp"

# define SQUARE_SIZE	20
# define BOUND_SIZE		10
# define GUI_HEIGHT		30

class DisplayerSfml2D : public IDisplayer
{
public:
	DisplayerSfml2D(void);
	~DisplayerSfml2D(void);

	void	init(int size_x, int size_y);
	void	close(void);

	void	put_score(const int score);
	void	put_framerate(const int framerate);
	void	put_entity(float x, float y, e_type type);
	void	put_time(int sec);
	e_key	get_input(void) const;
	void	clear_window(void);
	void	draw(void);

	class	InitError : public std::exception
	{
	public:
		virtual const char *	what(void) const noexcept;
	};

private:
	DisplayerSfml2D & operator=(DisplayerSfml2D const & rhs);
	DisplayerSfml2D(DisplayerSfml2D const & rhs);

	e_key	_getMoves(void) const;
	void	_putSnakeElem(float x, float y, const sf::Color & color);
	void	_putFood(float x, float y);
	void	_putSpecialFood(float x, float y);
	void	_put_title(void);

	Coord<float>		_map_size;
	Coord<int>			_win_size;
	sf::RenderWindow *	_win;
	sf::Font			_guiFont;

};

extern "C" IDisplayer * create(void);

#endif
