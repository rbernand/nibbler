#ifndef ENTITY_CLASS_HPP
# define ENTITY_CLASS_HPP

#include <Coord.tpp>
#include <conf.h>
#include <iostream>
#include <sstream>

typedef enum		e_type:char
{
	_type_none = -1,
	_type_snake = 0,
	_type_snake_head,
	_type_food,
	_type_special_food
}					t_type;

enum		e_dir
{
	_dir_right = 0,
	_dir_up,
	_dir_left,
	_dir_down,
};

class Entity
{
	public:
		Entity ( Entity const & src );
		Entity (float x, float y, t_type type, e_dir dir);
		virtual ~Entity (void) {};
		void			move( void );
		void			move( Coord<int> map_size );
		void			setDir (e_dir dir);
		e_type			getType( void ) const;
		e_dir			getDir ( void ) const;
		Coord<int>		getPos ( void ) const;
		float			getY ( void ) const;
		float			getX ( void ) const;
		void			updatePos ( float x, float y);
		bool			operator==( Entity const & rhs ) const;

	private:
		Entity &	operator=( Entity const & rhs );

		static unsigned int			_nid;
		const unsigned int			_id;
		Coord<int>					_pos;
		t_type						_type;
		e_dir						_dir;
};

std::ostream		&operator<<(std::ostream &o, Entity const &rhs );

#endif  /* ENTITY_CLASS_HPP */
