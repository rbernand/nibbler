#ifndef FOOD_CLASS_HPP
# define FOOD_CLASS_HPP

#include <iostream>
#include <sstream>

#include <Entity.class.hpp>

class Food : public Entity
{
	public:
		Food ( int x, int y );
		~Food (void);

	private:
		
};

std::ostream		&operator<<(std::ostream &o, Food const &rhs );

#endif  /* FOOD_CLASS_HPP */
