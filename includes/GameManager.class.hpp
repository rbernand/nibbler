// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   GameManager.class.hpp                              :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: caupetit <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/03/10 19:05:28 by caupetit          #+#    #+#             //
/*   Updated: 2015/04/06 16:53:42 by rbernand         ###   ########.fr       */
//                                                                            //
// ************************************************************************** //

#ifndef GAMEMANAGER_CLASS_HPP
# define GAMEMANAGER_CLASS_HPP

#include <iostream>
#include <sstream>
#include <dlfcn.h>
#include <exception>

#include <conf.h>
#include <IDisplayer.interface.hpp>
#include <IAudioManager.interface.hpp>
#include <DisplayerFactory.class.hpp>
#include <Coord.tpp>
#include <Snake.class.hpp>
#include <Chronotrigger.class.hpp>
#include <Food.class.hpp>
#include <SpecialFood.class.hpp>

#include <sstream>
#include <ctime>
#include <unistd.h>
#include <chrono>
#include <cmath>

# define CHRONO			std::chrono
# define STEADY_CLOCK	CHRONO::steady_clock

enum				e_collision
{
	_col_none = 0,
	_col_wall,
	_col_food,
	_col_sfood,
	_col_snake
};

enum				e_game_state
{
	_st_running = 1,
	_st_pause,
	_st_lose,
	_st_win,
	_st_player1_lose,
	_st_player2_lose,
	_st_unexpected_exit
};

class GameManager
{
	public:
		GameManager ( void );
		~GameManager (void);
		virtual void				init( int size_x, int size_y );
		virtual void				init( void ) {};
		virtual void				launch( void );

		class InvalidMapException : public std::exception
		{
			public:
				InvalidMapException (int x, int y);
				virtual const char *		what( void ) const noexcept;
			private:
				const int			_x;
				const int			_y;
		};

	protected:
		void				_updateDisplay( void ) const;
		IDisplayer *		_load_displayer( e_displayer displayer );
		void				_load_audio_lib( void );
		Food *				_pop_food ( e_type ) const;
		void				_switchDisplayer ( e_key key );
		virtual e_key		_manageInputs( void );
		void				_updateEntity( void );
		void				_foodEated ( e_collision col, Snake * snake);
		void				_switchPause ( e_key key );
		bool				_check_collisions_snake( Snake * snake, Entity const & ) const;

		ChronoTrigger			_timer;
		Coord<int>				_map_size;
		e_game_state			_state;
		IDisplayer *			_displayer;
		Food *					_food;
		Food *					_sfood;
		IAudioManager *			_audioManager;
		int						_score;

	private:
		GameManager &	operator=( GameManager const & rhs );
		GameManager ( GameManager const & src );
		void				_keyExit ( e_key key );
		void				_setDirSnake(e_key key);
		virtual void		_loop(void);
		int					_manageFramerate(void);
		void				_displaySnake( void ) const;
		e_collision			_check_all_collisions( void ) const;

		Snake *					_snake;
		e_displayer				_currentDispalyer;
		DisplayerFactory		_displayerFactory;
};

typedef void			(GameManager::*key_fct)(e_key);

#endif  /* GAMEMANAGER_CLASS_HPP */
