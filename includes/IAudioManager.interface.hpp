#ifndef IAUDIOMANAGER_INTERFACE_HPP
# define IAUDIOMANAGER_INTERFACE_HPP

class IAudioManager
{
	public:
		virtual ~IAudioManager(void) {}
		virtual void	init(void) = 0;
		virtual void	popFood ( void ) = 0;
};

typedef IAudioManager * (* t_audio)(void);

#endif
