#ifndef IDISPLAYER_INTERFACE_HPP
# define IDISPLAYER_INTERFACE_HPP

#include <Entity.class.hpp>

enum			e_key:int
{
	_key_invalid = -1,
	_key_right,
	_key_up,
	_key_left,
	_key_down,
	_key_exit,
	_key_lib1,
	_key_lib2,
	_key_lib3,
	_key_pause,
	_key_nb
};

enum			e_gui_info
{
	_inf_score,
	_inf_time,
	_inf_framerate,
	_inf_number
};

enum			e_displayer
{
	e_ncurses = 0,
	e_sfml2d,
	e_glfw
};

class IDisplayer
{
	public:
		virtual ~IDisplayer(void) {}
		virtual void	init(int size_x, int size_y) = 0;
		virtual void	close(void) = 0;

		virtual void	put_score(const int score) = 0;
		virtual void	put_time(int sec) = 0;
		virtual void    put_framerate(const int framerate) = 0;
		virtual void	put_entity(float x, float y, e_type type) = 0;
		virtual e_key	get_input(void) const = 0;
		virtual void	clear_window(void) = 0;
		virtual void	draw(void) = 0;
};

#endif  /* IDISPLAYER_INTERFACE_HPP */
