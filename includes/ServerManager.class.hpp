// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   ServerManager.class.hpp                            :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: caupetit <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/03/26 15:19:30 by caupetit          #+#    #+#             //
//   Updated: 2015/04/06 14:48:30 by caupetit         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef SERVERMANAGER_CLASS_HPP
# define SERVERMANAGER_CLASS_HPP

# include <iostream>
# include <sstream>
# include <netdb.h>

# include <sys/socket.h>
# include <netdb.h>
# include <unistd.h>
# include <thread>

# include <GameManager.class.hpp>
# include <SocketManager.class.hpp>

# define NB_MAX_PLAYER				2

typedef struct sockaddr_in	t_sock_in;
typedef struct sockaddr		t_sock;

class ServerManager : public SocketManager, public GameManager
{
	public:
		ServerManager (int port);
		~ServerManager ( void );
		void				init (int x, int y);
		void				launch( void );

	private:
		void				_check_all_collisions ( void );
		void				_sendNewClient ( int p );
		void				_updateEntity ( void );
		Food *				_pop_food ( e_type type) const;
		void				_loop ( void );
		void				_handle_colllisions (int id, e_collision col);
		e_collision			_col_one_player ( SnakeEntity const & head) const;
		int					_foodEated ( e_collision col , Snake * snake);
		void				_sendStatus ( void );
		void				_update_map(void);
		void				_updateDisplay(void);
		void				_add_snake_in_map(Snake * snake, e_type head, e_type body);
		void				_add_foods_in_map(void);
		static void			_communicate ( ServerManager * ref, int pIn, int id ) ;

		int					_c_socket[NB_MAX_PLAYER];
		t_sock_in			_sin;
		t_sock_in			_c_sin[NB_MAX_PLAYER];
		int					_s_socket;
		e_type *			_map;
		Snake *				_players[NB_MAX_PLAYER];
		unsigned int		_scores[NB_MAX_PLAYER];
};

std::ostream		&operator<<(std::ostream &o, ServerManager const &rhs );

#endif  /* SERVERMANAGER_CLASS_HPP */
