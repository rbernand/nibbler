#ifndef SNAKE_HPP
# define SNAKE_HPP

#include <iostream>
#include <sstream>

#include <SnakeEntity.class.hpp>
#include <conf.h>

typedef std::list<SnakeEntity> t_body;

class Snake
{
	public:
		Snake (float x, float y, e_dir dir);
		~Snake (void);

		std::string		toString ( void ) const;
		void			toString ( std::ostream & ) const;
		bool			move ( double deltaTime );
		void			grow( void );
		void			setDir(e_dir dir);
		void			setTmpDir(e_dir dir);
		t_body const &		getBody ( void ) const;
		SnakeEntity const &		getHead ( void ) const;
		void					increaseSpeed ( void );

	private:
		Snake ( void );
		Snake ( Snake const & src );
		Snake &	operator=( Snake const & rhs );

		t_body								_body;
		e_dir								_dir;
		e_dir								_tmp_dir;
		double								_speed;
		double								_step;
};

#endif  /* SNAKE_HPP */
