#ifndef SNAKEENTITY_HPP
# define SNAKEENTITY_HPP

#include <iostream>
#include <sstream>
#include <list>

#include <Entity.class.hpp>

class SnakeEntity : public Entity
{
	public:
		SnakeEntity (float x, float y, e_dir dir);
		~SnakeEntity (void);
		SnakeEntity &	operator=( SnakeEntity const & rhs );

	private:
};

#endif  /* SNAKEENTITY_HPP */
