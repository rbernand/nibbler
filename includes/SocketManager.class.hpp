#ifndef SOCKETMANAGER_CLASS_HPP
# define SOCKETMANAGER_CLASS_HPP

# include <sys/socket.h>
# include <mutex>

class SocketManager
{
public:
	SocketManager(void);
	~SocketManager(void);
	bool		_datarecv(int sock, void*ptr, int si, int i);
	void		_datasend(int sock, const void *ptr, int si, int i);
	void		_datasend(int sock, void *ptr, int si, int i);

	static bool		_valid;
private:
	SocketManager(const SocketManager & src);
	SocketManager & operator=(const SocketManager & rhs);
};

#endif
