#ifndef SPECIALFOOD_CLASS_HPP
# define SPECIALFOOD_CLASS_HPP

#include <iostream>
#include <sstream>

#include <Food.class.hpp>

class SpecialFood : public Food
{
	public:
		SpecialFood ( float x, float y);
		~SpecialFood (void);
		void			decreaseLife ( void );
		int				getLife ( void ) const;

	private:
		int					_life;
};

std::ostream		&operator<<(std::ostream &o, SpecialFood const &rhs );

#endif  /* SPECIALFOOD_CLASS_HPP */
