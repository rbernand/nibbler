#ifndef CONF_H
#define CONF_H

# define			DEFAULT_DISPLAYER			e_ncurses
# define			NB_LIB						3
# define			MAP_X_MIN					20
# define			MAP_X_MAX					50
# define			MAP_Y_MIN					20
# define			MAP_Y_MAX					50
# define			START_SIZE					4

# define			ASSETS_FOLDER				"assets"
# define			FONT_NAME					"KingOfTheHill.ttf"

# define			FRAME_PER_SEC				60

# define			DEFAULT_PORT				4242

# define			START						654
# define			END							456

# define ECHO(X)		std::cout << X << std::endl
# define DEBUG				std::cout << "["<< __FUNCTION__ << ":" <<__LINE__ << "]" << std::endl;

#endif /* !CONF_H */
