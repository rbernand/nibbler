#include "Chronotrigger.class.hpp"

# include <iostream> // DELETE

ChronoTrigger::ChronoTrigger(void):
	_fpsLimitTime(1000000.0 / 60),
	_frameDuration(0),
	_frameNb(0),
	_framerate(0)
{
	std::cout << "ChronoTrigger created" << std::endl;
	_begin = STEADY_CLOCK::now();
	_lastFrame = _begin;
	_framePoint = _begin;
}

ChronoTrigger::ChronoTrigger(int fps): _fpsLimitTime(1000000.0 / fps)
{
	std::cout << "ChronoTrigger created" << std::endl;
	_begin = STEADY_CLOCK::now();
	_lastFrame = _begin;
	_framePoint = _begin;
}

ChronoTrigger::~ChronoTrigger(void)
{
	std::cout << "ChronoTrigger destructed" << std::endl;
}

void	ChronoTrigger::manageFramerate(void)
{
	auto now = STEADY_CLOCK::now();
	fpsTime sleepTime;

	_frameNb++;
	auto frameTime = now - _lastFrame;
	if (frameTime <= _fpsLimitTime)
	{
		sleepTime = _fpsLimitTime - frameTime;
		usleep(sleepTime.count() - 1);
	}
	_setFramerate(now);
	_frameDuration = sleepTime + frameTime;
	_lastFrame = STEADY_CLOCK::now();
}

void	ChronoTrigger::_setFramerate(STEADY_CLOCK::time_point now)
{
	fpsTime timeSinceLastPoint = now - _framePoint;
	if (timeSinceLastPoint >= CHRONO::seconds(1))
	{
		_framerate = _frameNb;
		_frameNb = 0;
		_framePoint = STEADY_CLOCK::now();
	}
}

double	ChronoTrigger::getDeltaTime(void) const
{
	return _frameDuration.count() / 1000000;
}

int		ChronoTrigger::getFramerate(void) const
{
	return _framerate;
}

void	ChronoTrigger::setGameBegin(void)
{
	_gameBegin = STEADY_CLOCK::now();
	_lastFrame = _gameBegin;
	_framePoint = _gameBegin;
}

void	ChronoTrigger::setFpsLimit(float limit)
{
	_fpsLimitTime = fpsTime(1000000 / limit);
}

int			ChronoTrigger::getTimeElapsed ( void ) const 
{
	auto now = STEADY_CLOCK::now();

	auto timeElapsed = now - _gameBegin;
	return (CHRONO::duration_cast<CHRONO::seconds>(timeElapsed).count());
}
