// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   ClientManager.class.cpp                            :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: caupetit <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/03/26 15:21:25 by caupetit          #+#    #+#             //
/*   Updated: 2015/04/06 17:16:33 by rbernand         ###   ########.fr       */
//                                                                            //
// ************************************************************************** //

#include <ClientManager.class.hpp>

ClientManager::~ClientManager ( void )
{
	::close(_sock);
}

/*
void	datarecv(int sock, void *ptr, int si, int i);
void	datasend(int sock, void *ptr, int si, int i);
void	datasend(int sock, const void *ptr, int si, int i);
*/
/* void	datarecv(int sock, void *ptr, int si, int i) */
/* { */
/* 	(void)i; */
/* 	int				r = 0; */

/* 	while (r < si) */
/* 	{ */
/* 		r += recv(sock, static_cast<char *>(ptr) + r, si - r, 0); */
/* 	} */
/* } */

ClientManager::ClientManager ( std::string info ) :
	GameManager()
{
	std::string		machine;
	int				port;
	std::string		tmp;
	size_t			pos;
	int				data;

	if ((pos = info.rfind(":")) == std::string::npos)
	{
		port = DEFAULT_PORT;
		machine = info;
	}
	else
	{
		machine = info.substr(0, pos);
		tmp = info.substr(pos + 1);
		port = atoi(tmp.c_str());
	}
	if ((_sock = socket(PF_INET, SOCK_STREAM, 0)) == -1)
		throw "Cannot create Socket" ;
	if ((_host_info = gethostbyname(machine.c_str())) == NULL)
		throw "host fail";
	_sin.sin_addr = *(struct in_addr *)_host_info->h_addr;
	_sin.sin_port = htons(port);
	_sin.sin_family = PF_INET;
	if (connect(_sock, (struct sockaddr *)&_sin, sizeof(struct sockaddr)) == -1)
		throw "connexion impossible";
	_datarecv(_sock, &data, sizeof(int), 0);
	_map_size.x = data;
	_datarecv(_sock, &data, sizeof(int), 0);
	_map_size.y = data;
	GameManager::_load_audio_lib();
}

void							ClientManager::init ( void )
{
	_timer.setFpsLimit(FRAME_PER_SEC);
	_displayer = _load_displayer(DEFAULT_DISPLAYER);
	_displayer->init(_map_size.x, _map_size.y);
}

void							ClientManager::_loop( void ) 
{
	int					ret = END;

	_displayer->draw();
	usleep(100);
	while (_state < _st_lose)
	{
		_displayer->clear_window();
		_updateDisplay();
		_getStatus();
		_displayer->draw();
		_manageInputs();
	}
	_displayer->close();
	if (_state == _st_unexpected_exit)
		ECHO("Server not reponsding");
	else if (_state == _st_win)
		ECHO("you win : " << _score);
	else if (_state == _st_lose)
		ECHO("you lose : " << _score);
	else
		ECHO("state : " << _state);
	_datasend(_sock, &ret, sizeof(int), 0);
}

void							ClientManager::_getStatus(void)
{
	int				time;

	if (!_datarecv(_sock, &_state, sizeof(e_game_state), 0))
		_state = _st_unexpected_exit;
	if (!_datarecv(_sock, &_score, sizeof(int), 0))
		_state = _st_unexpected_exit;
	if (!_datarecv(_sock, &time, sizeof(int), 0))
		_state = _st_unexpected_exit;
	_displayer->put_time(time);
}

e_key							ClientManager::_manageInputs ( void )
{
	e_key			key = _displayer->get_input();

	if (key >= _key_lib1 && key <= _key_lib3)
		_switchDisplayer(key);
	if (key != _key_invalid)
		_datasend(_sock, &key, sizeof(e_key), 0);
	return (key);
}

void							ClientManager::_updateDisplay ( void )
{
	static int						size = _map_size.x * _map_size.y;
	static e_type *					map = new e_type[size];

	_datarecv(_sock, map, size * sizeof(e_type), 0);
	for (int i =  0 ; i < size ; i++)
	{
		if (map[i] != _type_none)
			_displayer->put_entity(i % _map_size.x, i / _map_size.x, map[i]);
	}
	_displayer->put_score(_score);
}

void							ClientManager::launch ( void )
{
	int					ret = 0;

	while (ret != START)
		_datarecv(_sock, &ret, sizeof(int), 0);
	_timer.setGameBegin();
	_audioManager->init();
	_loop();
}
