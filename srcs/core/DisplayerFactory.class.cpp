#include <DisplayerFactory.class.hpp>

const char *		DisplayerFactory::_path[] = {
	"./lib/libncurses.dll",
	"./lib/libsfml2d.dll",
	"./lib/libglfw.dll"
};

DisplayerFactory::DisplayerFactory ( void )
{
	std::cout << "Factory Created" << std::endl;
	return ;
}

DisplayerFactory::~DisplayerFactory (void)
{
	std::cout << "Factory Desctructed" << std::endl;
	return ;
}

#include <unistd.h>

IDisplayer *		DisplayerFactory::load(e_displayer type)
{
	t_create				create;
	IDisplayer *			displayer;
	void *					handler;

	if ((handler = dlopen(_path[type], RTLD_LAZY)) == NULL)
		throw(DisplayerFactory::InvalidDisplayer(dlerror()));
	create = reinterpret_cast<t_create>(dlsym(handler, "create"));
	if (create == NULL)
		throw(DisplayerFactory::InvalidDisplayer(dlerror()));
	displayer = create();
	if (!displayer)
		throw(DisplayerFactory::InvalidDisplayer("Null displayer created"));
	return (displayer);
}

DisplayerFactory::InvalidDisplayer::InvalidDisplayer ( std::string msg) : _msg(msg)
{}

const char *	DisplayerFactory::InvalidDisplayer::what( void ) const noexcept
{
	ECHO(_msg);
	return (("InvalidDisplayer: " + _msg).c_str());
}
