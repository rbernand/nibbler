#include <Entity.class.hpp>

unsigned int Entity::_nid = 0;

Entity::Entity ( const Entity & rhs ) :
	_id(Entity::_nid)
{
	Entity::_nid++;
	*this = rhs;
}

Entity::Entity (float x, float y, t_type type, e_dir dir) :
	_id(Entity::_nid),
	_pos(x, y),
	_type(type),
	_dir(dir)
{
	Entity::_nid++;
}

Entity &	Entity::operator=( Entity const & rhs )
{
	_pos = rhs._pos;
	_dir = rhs._dir;
	_type = rhs._type;
	return *this;
}

bool		Entity::operator==( Entity const & rhs ) const
{
	if (_id == rhs._id)
		return (true);
	return (false);
}

void		Entity::setDir ( e_dir dir)
{
	_dir = dir;
}

e_dir		Entity::getDir ( void ) const
{
	return (_dir);
}

void		Entity::move( void )
{
	if (_dir == _dir_up)
		_pos.y -= 1;
	else if (_dir == _dir_down)
		_pos.y += 1;
	else if (_dir == _dir_right)
		_pos.x += 1;
	else if (_dir == _dir_left)
		_pos.x -= 1;
}

void		Entity::move( Coord<int> map_size )
{
	if (_dir == _dir_up)
		_pos.y = _pos.y - 1 <= 0 ? map_size.y : _pos.y - 1;
	else if (_dir == _dir_down)
		_pos.y = _pos.y + 1 > map_size.y ? 0 : _pos.y + 1;
	else if (_dir == _dir_right)
		_pos.x = _pos.x + 1 > map_size.x ? 0 : _pos.x + 1;
	else if (_dir == _dir_left)
		_pos.x = _pos.x - 1 <= 0 ? map_size.x : _pos.x - 1;
}

void		Entity::updatePos ( float x, float y)
{
	_pos.x += x;
	_pos.y += y;
}

Coord<int>	Entity::getPos ( void ) const
{
	return (_pos);
}

float		Entity::getX ( void ) const
{
	return (_pos.x);
}

float		Entity::getY ( void ) const
{
	return (_pos.y);
}
