// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   GameManager.class.cpp                              :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: caupetit <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/03/25 18:28:50 by caupetit          #+#    #+#             //
/*   Updated: 2015/04/06 18:12:27 by rbernand         ###   ########.fr       */
//                                                                            //
// ************************************************************************** //

#include <GameManager.class.hpp>
#include <IDisplayer.interface.hpp>

GameManager::GameManager ( void ) :
	_state(_st_pause),
	_displayer(NULL),
	_food(NULL),
	_sfood(NULL),
	_score(0),
	_snake(NULL),
	_currentDispalyer(DEFAULT_DISPLAYER)
{
	std::srand(std::time(NULL));
	return ;
}

GameManager::~GameManager (void)
{
	std::cout << "GameManaged destructed" << std::endl;
	return ;
}

void			GameManager::init(int size_x, int size_y)
{
	_timer.setFpsLimit(FRAME_PER_SEC);
	if (size_x < MAP_X_MIN || size_x > MAP_X_MAX
			|| size_y < MAP_Y_MIN || size_y > MAP_Y_MAX)
		throw GameManager::InvalidMapException(size_x, size_y);
	_map_size.x = size_x;
	_map_size.y = size_y;
	_snake = new Snake(size_x / 2, size_y / 2, _dir_left);
	_displayer = _load_displayer(_currentDispalyer);
	_displayer->init(_map_size.x, _map_size.y);	
	_load_audio_lib();
}

void			GameManager::launch(void)
{
	_food = _pop_food(_type_food);
	_updateDisplay();
	_displayer->draw();
	_audioManager->init();
	_timer.setGameBegin();
	_loop();
	ECHO("Final Score : " << _score);
}

void			GameManager::_loop(void)
{
	e_collision			ret_col;

	while (_state == _st_running || _state == _st_pause)
	{
		if (_state == _st_running)
		{
			_updateEntity();
			ret_col = _check_all_collisions();
			if (ret_col == _col_food || ret_col == _col_sfood)
				_foodEated(ret_col, _snake);
			else if (ret_col != _col_none)
				_state = _st_lose;
		}
		if (_state >= _st_lose)
			break ;
		_updateDisplay();
		_displayer->draw();
		_manageInputs();
		_timer.manageFramerate();
	}
	_displayer->close();
}

void			GameManager::_foodEated ( e_collision col , Snake * snake)
{
	if (col == _col_food)
	{
		delete _food;
		_food = _pop_food(_type_food);
		_score++;
	}
	else if (col == _col_sfood)
	{
		_score += static_cast<SpecialFood *>(_sfood)->getLife() / 10;
		delete _sfood;
		_sfood = NULL;
		snake->increaseSpeed();
		snake->grow();
	}
	snake->increaseSpeed();
	snake->grow();
	_audioManager->popFood();
}

void			GameManager::_updateEntity ( void )
{
	if (_sfood == NULL && rand() % (FRAME_PER_SEC * 5) == 0)
		_sfood = _pop_food(_type_special_food);
	else if (_sfood != NULL)
	{
		static_cast<SpecialFood *>(_sfood)->decreaseLife();
		if (static_cast<SpecialFood *>(_sfood)->getLife() == 0)
		{
			delete _sfood;
			_sfood = NULL;
		}
	}
	_snake->move(_timer.getDeltaTime());
}

void			GameManager::_displaySnake( void ) const
{
	t_body::const_iterator		it;

	it = _snake->getBody().begin();
	_displayer->put_entity(std::floor((*it).getX()),
			std::floor((*it).getY()), _type_snake_head);
	// start snake and food loop will come
	it++;
	while (it != _snake->getBody().end())
	{
		_displayer->put_entity(std::floor((*it).getX()),
				std::floor((*it).getY()), _type_snake);
		it++;
	}
}

void			GameManager::_updateDisplay(void) const
{
	_displayer->clear_window();
	_displaySnake();
	_displayer->put_entity(_food->getX(), _food->getY(), _type_food);
	if(_sfood != NULL)
		_displayer->put_entity(_sfood->getX(), _sfood->getY(), _type_special_food);
	_displayer->put_framerate(_timer.getFramerate());
	_displayer->put_score(_score);
	_displayer->put_time(_timer.getTimeElapsed());
}

void			GameManager::_switchDisplayer ( e_key key)
{
	e_displayer				newDisplayer = static_cast<e_displayer>(key - _key_lib1);

	if (_currentDispalyer != newDisplayer)
	{
		_currentDispalyer = newDisplayer;
		_displayer = _load_displayer(newDisplayer);
		_displayer->init(_map_size.x, _map_size.y);
	}
	_state = _st_pause;
}

void			GameManager::_setDirSnake(e_key key)
{
	_snake->setTmpDir(static_cast<e_dir>(key));
}

void			GameManager::_switchPause(e_key key)
{
	(void)key;
	_state = _state == _st_pause ? _st_running : _st_pause;
}

void			GameManager::_keyExit ( e_key key)
{
	(void)key;
	_state = _st_lose;
}

e_key			GameManager::_manageInputs(void)
{
	e_key			key;
	static key_fct		exec_key[] = {
		&GameManager::_setDirSnake,
		&GameManager::_setDirSnake,
		&GameManager::_setDirSnake,
		&GameManager::_setDirSnake,
		&GameManager::_keyExit,
		&GameManager::_switchDisplayer,
		&GameManager::_switchDisplayer,
		&GameManager::_switchDisplayer,
		&GameManager::_switchPause,
	};

	key = _displayer->get_input();
	if ( key == _key_invalid )
		return (key);
	(this->*(exec_key[key])) (key);
	return (key);
}

void			GameManager::_load_audio_lib(void)
{
	void			*handler;
	t_audio			create;

	if ((handler = dlopen("./lib/libaudio.dll", RTLD_LAZY)) == NULL)
	{
		std::cout << dlerror() << std::endl;
		throw std::exception();
	}
	create = reinterpret_cast<t_audio>(dlsym(handler, "create"));
	if (!create)
	{
		std::cout << dlerror() << std::endl;
		throw std::exception();
	}
	_audioManager = create();
	if (!_audioManager)
	{
		std::cout << "create failed" << std::endl;
		throw std::exception();
	}
}

IDisplayer *	GameManager::_load_displayer(e_displayer displayer)
{
	if (_displayer != NULL)
	{
		_displayer->close();
		delete _displayer;
	}
	return (_displayerFactory.load(displayer));
}

e_collision		GameManager::_check_all_collisions ( void ) const
{
	SnakeEntity const &					head = _snake->getHead();

	if (head.getX() < 0 || head.getX() >= _map_size.x)
		return (_col_wall);
	else if (head.getY() < 0 || head.getY() >= _map_size.y)
		return (_col_wall);
	if (_check_collisions_snake(_snake, head))
		return (_col_snake);
	if (_check_collisions_snake(_snake, *_food))
		return (_col_food);
	if (_sfood != NULL && _check_collisions_snake(_snake, *_sfood))
		return (_col_sfood);
	return (_col_none);
}

Food *			GameManager::_pop_food ( e_type type ) const
{
	Coord<float>		pos;
	Food *				food;

	pos.x = rand() % (_map_size.x - 1) + 1;
	pos.y = rand() % (_map_size.y - 1) + 1;
	if (type == _type_special_food)
		food = new SpecialFood(pos.x, pos.y);
	else
		food = new Food(pos.x, pos.y);
	if (_check_collisions_snake(_snake, *food)
			|| (_food != NULL && food->getPos() == _food->getPos())
			|| (_sfood != NULL &&food->getPos() == _sfood->getPos()))
	{
		delete food;
		return (_pop_food(type));
	}
	return (food);
}

bool			GameManager::_check_collisions_snake ( Snake * snake, Entity const & target ) const
{
	t_body const & 						body = snake->getBody();
	t_body::const_iterator		it = body.begin();
	t_body::const_iterator		ite = body.end();

	if (static_cast<Entity const &>(*it) == target)
		it++;
	while (it != ite)
	{
		if (target.getPos() == (*it).getPos())
			return (true);
		it++;
	}
	return (false);
}

GameManager::InvalidMapException::InvalidMapException(int x, int y) : _x(x), _y(y)
{}

const char *GameManager::InvalidMapException::what(void) const noexcept
{
	std::stringstream	ss;

	ss << "Invalid Map Exception: " << _x << " " << _y;
	return (ss.str().c_str());
}
