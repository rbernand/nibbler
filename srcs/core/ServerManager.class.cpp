// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   ServerManager.class.cpp                            :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: caupetit <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/03/26 15:02:31 by caupetit          #+#    #+#             //
/*   Updated: 2015/04/06 17:11:23 by rbernand         ###   ########.fr       */
//                                                                            //
// ************************************************************************** //

#include <ServerManager.class.hpp>

ServerManager::ServerManager (int port ) :
	SocketManager(), GameManager()
{
	_c_socket[0] = -1;
	_c_socket[1] = -1;
	_scores[0] = 0;
	_scores[1] = 0;
	if ((_s_socket = socket(PF_INET, SOCK_STREAM, 0)) == -1)
		throw "socket ca pue ";
	_sin.sin_addr.s_addr = htonl(INADDR_ANY);
	_sin.sin_family = AF_INET;
	_sin.sin_port = htons(port);
	if (bind(_s_socket, (t_sock *)&_sin, sizeof(t_sock_in)) == -1)
		throw "bind failed";
	if (listen(_s_socket, 1 + NB_MAX_PLAYER) == -1)
		throw "listen failed";
	(void)_c_sin;
}

ServerManager::~ServerManager ( void )
{
	for (int i = 0; i < NB_MAX_PLAYER; i++)
		::close(_c_socket[i]);
	::close(_s_socket);
}

void						ServerManager::launch ( void )
{
	int						i = 0;
	socklen_t				sin_size;

	std::cout << "launched" << std::endl;
	while (_c_socket[0] == -1 || _c_socket[1] == -1)
	{
		ECHO("Waiting for Player " << i + 1);
		if ((_c_socket[i] = accept(_s_socket, (t_sock *)&_sin, &sin_size)) != -1)
		{
			i++;
			ECHO("Player " << i << " connected.");
		}
		else
			throw "accept failed";
		if (i == NB_MAX_PLAYER)
			break ;
	}
	ECHO("GAME STARTED");
	_timer.setGameBegin();
	_loop();
}

void						ServerManager::init(int size_x, int size_y)
{
	_timer.setFpsLimit(FRAME_PER_SEC);
	if (size_x < MAP_X_MIN || size_x > MAP_X_MAX
			|| size_y < MAP_Y_MIN || size_y > MAP_Y_MAX)
		throw GameManager::InvalidMapException(size_x, size_y);
	_map_size.x = size_x;
	_map_size.y = size_y;
	_map = new e_type[size_y * size_x];
	_players[0] = new Snake(_map_size.x / 2 - 1, _map_size.y /2 - 1, _dir_left);
	_players[1] = new Snake(_map_size.x / 2 + 1, _map_size.y /2 + 1, _dir_right);
	std::cout << _players[0] << "  " <<_players[1] << std::endl;
	_food = _pop_food(_type_food);
	_update_map();
}

void						ServerManager::_sendNewClient ( int p )
{
	this->_datasend(_c_socket[p], &_map_size.x, sizeof(int), 0);
	this->_datasend(_c_socket[p], &_map_size.y, sizeof(int), 0);
}

void						ServerManager::_communicate ( ServerManager * ref, int pIn, int id)
{
	int			start = START;
	e_key		key = _key_invalid;

	usleep(3000);
	ref->_datasend(pIn, &start, sizeof(int), 0);
	while (key != _key_exit)
	{
		if (!ref->_datarecv(pIn, &key, sizeof(e_key), 0))
		{
			::close(ref->_c_socket[0]);
			::close(ref->_c_socket[1]);
			ref->_state = _st_lose;
			break;
		}
		if (key >= _key_right && key <= _key_down)
			ref->_players[id]->setTmpDir(static_cast<e_dir>(key));
		if (key >= _key_lib1 && key <= _key_lib3)
			ref->_state = _st_pause;
		if (key == _key_pause)
			ref->_switchPause(key);
		if (key == _key_exit || !SocketManager::_valid)
		{
			::close(ref->_c_socket[0]);
			::close(ref->_c_socket[1]);
			ref->_state = _st_lose;
			break;
		}
	}
}

void						ServerManager::_updateEntity ( void )
{
	if (_sfood == NULL && rand() % (FRAME_PER_SEC * 5) == 0)
		_sfood = _pop_food(_type_special_food);
	else if (_sfood != NULL)
	{
		static_cast<SpecialFood *>(_sfood)->decreaseLife();
		if (static_cast<SpecialFood *>(_sfood)->getLife() == 0)
		{
			delete _sfood;
			_sfood = NULL;
		}
	}
	_players[0]->move(_timer.getDeltaTime());
	_players[1]->move(_timer.getDeltaTime());
}

Food *						ServerManager::_pop_food ( e_type type ) const
{
	Coord<float>		pos;
	Food *				food;

	pos.x = rand() % (_map_size.x - 1) + 1;
	pos.y = rand() % (_map_size.y - 1) + 1;
	if (type == _type_special_food)
		food = new SpecialFood(pos.x, pos.y);
	else
		food = new Food(pos.x, pos.y);
	if (_check_collisions_snake(_players[0], *food)
			|| _check_collisions_snake(_players[1], *food)
			|| (_food != NULL && food->getPos() == _food->getPos())
			|| (_sfood != NULL &&food->getPos() == _sfood->getPos()))
	{
		delete food;
		return (_pop_food(type));
	}
	return (food);
}

e_collision					ServerManager::_col_one_player ( SnakeEntity const & head) const
{
	if (head.getX() < 0 || head.getX() >= _map_size.x)
		return (_col_wall);
	else if (head.getY() < 0 || head.getY() >= _map_size.y)
		return (_col_wall);
	for (int i = 0; i < NB_MAX_PLAYER; i++)
	{
		if (_check_collisions_snake(_players[i], head))
			return (_col_snake);
	}
	if (head.getPos() == _food->getPos())
		return (_col_food);
	if (_sfood && head.getPos() == _sfood->getPos())
		return (_col_sfood);
	return (_col_none);
}

void						ServerManager::_handle_colllisions (int id, e_collision col)
{
	if (col == _col_food || col == _col_sfood)
		_scores[id] += _foodEated(col, _players[id]);
	else if (col == _col_wall || col == _col_snake)
		_state = static_cast<e_game_state>(_st_player1_lose + id);
}

void						ServerManager::_check_all_collisions ( void )
{
	e_collision				ret;

	for (int i = 0; i < NB_MAX_PLAYER; i++)
	{
		ret = _col_one_player(_players[i]->getHead());
		if (ret != _col_none)
			_handle_colllisions(i, ret);
	}
}

void						ServerManager::_loop ( void )
{
	std::thread			player1(ServerManager::_communicate, this, _c_socket[0], 0);
	_sendNewClient(0);
	_datasend(_c_socket[0], _map, _map_size.x * _map_size.y * sizeof(e_type), 0);
	std::thread			player2(ServerManager::_communicate, this, _c_socket[1], 1);
	_sendNewClient(1);
	_datasend(_c_socket[1], _map, _map_size.x * _map_size.y * sizeof(e_type), 0);

	while (_state == _st_running || _state == _st_pause)
	{
		if (_state == _st_running)
		{
			_updateEntity();
			_check_all_collisions();
		}
		if (_state == _st_lose)
			break ;
		_update_map();
		_updateDisplay();
		_sendStatus();
		_timer.manageFramerate();
	}

	_updateDisplay();
	_sendStatus();
	player1.join();
	player2.join();
}

int							ServerManager::_foodEated ( e_collision col , Snake * snake)
{
	int				s = 0;

	if (col == _col_food)
	{
		delete _food;
		_food = _pop_food(_type_food);
		s = 1;
	}
	else if (col == _col_sfood)
	{
		s = static_cast<SpecialFood *>(_sfood)->getLife() / 10;
		delete _sfood;
		_sfood = NULL;
		snake->grow();
	}
	snake->increaseSpeed();
	snake->grow();
	return (s);
}

void			ServerManager::_sendStatus ( void )
{
	int			time = _timer.getTimeElapsed();
	for (int i = 0; i < NB_MAX_PLAYER; i++)
	{
		if (_state >= _st_lose)
		{
			if (_scores[i] > _scores[(i + 1) % 2])
				_state = _st_win;
			else
				_state = _st_lose;
			_datasend(_c_socket[i], &_state, sizeof(e_game_state), 0);
		}
		else
			_datasend(_c_socket[i], &_state, sizeof(e_game_state), 0);
		_datasend(_c_socket[i], &_scores[i], sizeof(int), 0);
		_datasend(_c_socket[i], &time, sizeof(int), 0);
	}
}

void			ServerManager::_update_map(void)
{
	std::memset(_map, -1, _map_size.x * _map_size.y);
	_add_snake_in_map(_players[0], _type_snake_head, _type_snake);
	_add_snake_in_map(_players[1], _type_snake, _type_snake_head);
	_add_foods_in_map();
}

void			ServerManager::_add_snake_in_map(Snake * snake, e_type head, e_type body)
{
	auto	it = snake->getBody().begin();
	int		x;
	int		y;
	e_type	type = head;

	while (it != snake->getBody().end())
	{
		x = std::floor(it->getX());
		y = std::floor(it->getY());
		_map[y * _map_size.x + x] = type;
		type = body;
		it++;
	}
}

void			ServerManager::_add_foods_in_map(void)
{
	int x = std::floor(_food->getX());
	int y = std::floor(_food->getY());
	_map[y * _map_size.x + x] = _type_food;
	if (!_sfood)
		return;
	x = std::floor(_sfood->getX());
	y = std::floor(_sfood->getY());
	_map[y * _map_size.x + x] = _type_special_food;
}

void		ServerManager::_updateDisplay(void)
{
	_datasend(_c_socket[0], _map, _map_size.x * _map_size.y * sizeof(e_type), 0);
	_datasend(_c_socket[1], _map, _map_size.x * _map_size.y * sizeof(e_type), 0);
}
