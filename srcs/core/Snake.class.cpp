#include <Snake.class.hpp>

Snake::Snake (float x, float y, e_dir dir) :
	_body(),
	_dir(dir),
	_tmp_dir(dir),
	_speed(0.1),
	_step(0)
{
	int				i = 1;
	SnakeEntity		head(x, y, dir);

	_body.push_back(head);
	while (i++ < START_SIZE)
		grow();
}

Snake::~Snake (void)
{
	std::cout << "Snake destructed" << std::endl;
	return ;
}

bool			Snake::move ( double deltaTime )
{
	t_body::reverse_iterator			it = _body.rbegin();
	t_body::reverse_iterator			itp;
	t_body::reverse_iterator			ite = _body.rend();

	if (_step < _speed)
		_step += deltaTime;
	else
	{
		this->setDir(_tmp_dir);
		while (it != ite)
		{
			(*it).move();
			itp = it;
			it++;
			if (it != ite)
				(*itp).setDir((*it).getDir());
		}
		_step = 0;
	}
	return (true);
}

void			Snake::grow( void )
{
	SnakeEntity			new_elem(_body.back());

	if (new_elem.getDir() == _dir_up)
		new_elem.updatePos(0, 1);
	if (new_elem.getDir() == _dir_down)
		new_elem.updatePos(0, -1);
	if (new_elem.getDir() == _dir_left)
		new_elem.updatePos(1, 0);
	if (new_elem.getDir() == _dir_right)
		new_elem.updatePos(-1, 0);
	_body.push_back(new_elem);
}

void			Snake::setDir(e_dir dir)
{
	if (dir % 2 == _dir % 2)
		return ;
	else
	{
		_dir = dir;
		_body.front().setDir(dir);
	}
}

void			Snake::increaseSpeed( void )
{
	_speed -= 0.005;
	_speed = _speed < 0.030 ? 0.030 : _speed;
}

void			Snake::setTmpDir(e_dir dir)
{
	_tmp_dir = dir;
}

t_body const &			Snake::getBody( void ) const
{
	return ( _body );
}

SnakeEntity const &		Snake::getHead( void ) const
{
	return (_body.front());
}
