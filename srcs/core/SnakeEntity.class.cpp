#include <SnakeEntity.class.hpp>

SnakeEntity::SnakeEntity ( float x, float y , e_dir dir) :
	Entity(x, y, _type_snake, dir)
{
	return ;
}

SnakeEntity::~SnakeEntity (void)
{
	return ;
}

SnakeEntity &	SnakeEntity::operator=( SnakeEntity const & rhs )
{
	*this = rhs;
	return *this;
}
