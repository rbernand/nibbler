#include "SocketManager.class.hpp"

SocketManager::SocketManager(void)
{
	return ;
}

SocketManager::~SocketManager(void)
{
	return;
}

bool		SocketManager::_datarecv(int sock, void*ptr, int si, int i)
{
	int				r = 0;
	int				d;

	while (r < si)
	{
		if (_valid)
			d = recv(sock, static_cast<char *>(ptr) + r, si - r, i);
		if (d == -1 || d == 0)
		{
			return false;
			_valid = false;
		}
		r += d;
	}
	return true;
}

void		SocketManager::_datasend(int sock, const void *ptr, int si, int i)
{
	int				r = 0;
	int				d;

	while (r < si)
	{
		if (_valid)
			d = send(sock, static_cast<char const *>(ptr) + r, si - r, i);
		if (d == -1 || d == 0)
		{
			_valid = false;
		}
		r += d;
	}
}

void		SocketManager::_datasend(int sock, void *ptr, int si, int i)
{
	int				r = 0;
	int				d;

	while (r < si)
	{
		if (_valid)
			d = send(sock, static_cast<char *>(ptr) + r, si - r, i);
		if (d == -1 || d == 0)
			_valid = false;
		r += d;
	}
}

bool		SocketManager::_valid = true;
