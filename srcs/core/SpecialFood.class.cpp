#include <SpecialFood.class.hpp>

SpecialFood::SpecialFood ( float x, float y ) :
	Food (x, y),
	_life(FRAME_PER_SEC * 3)
{
	return ;
}

SpecialFood::~SpecialFood (void)
{
	return ;
}

void			SpecialFood::decreaseLife ( void )
{
	_life--;
}

int				SpecialFood::getLife ( void ) const
{
	return (_life);
}

