#include <stdlib.h>

#include <conf.h>
#include <GameManager.class.hpp>
#include <ServerManager.class.hpp>
#include <ClientManager.class.hpp>
#include <DisplayerFactory.class.hpp>

int			main(int argc, char **argv)
{
	GameManager		*manager;

	try
	{
		if (argc == 2)
		{
			manager = new ClientManager(argv[1]);
			manager->init();
		}
		else if (argc == 4)
		{
			manager = new ServerManager(atoi(argv[3]));
			manager->init(atoi(argv[1]), atoi(argv[2]));
		}
		else if (argc == 3)
		{
			manager = new GameManager();
			manager->init(atoi(argv[1]), atoi(argv[2]));
		}
		else
		{
			manager = new GameManager();
			manager->init(MAP_X_MIN, MAP_Y_MIN);
		}
		manager->launch();
	}
	catch (std::exception & e) {
		ECHO("Error catched" << std::endl << e.what());
	}
	catch (const char * msg) {
		ECHO("Error catched" << std::endl << msg);
	}
	catch (...) {
		ECHO("Error");
	}
	return (0);
}
