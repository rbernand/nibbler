#include <AudioManager.class.hpp>

AudioManager::AudioManager(void)
{
	std::cout << "AudioManager Created" << std::endl;
	return;
}

AudioManager::~AudioManager(void)
{
	_mainTheme.stop();
	std::cout << "AudioManager Deleted" << std::endl;
	return;
}

void		AudioManager::init(void)
{
	if (!_mainTheme.openFromFile("./assets/got.ogg"))
		throw InitError("openning main theme");
	if (!_sBuff.loadFromFile("./assets/onTime.wav"))
		throw InitError("openning popsound");
	_popfood.setBuffer(_sBuff);
	_mainTheme.play();
	_mainTheme.setLoop(true);
}

void		AudioManager::popFood ( void )
{
	_mainTheme.setPitch(_mainTheme.getPitch() + 0.1);
	_popfood.play();
}

AudioManager::InitError::InitError(void): _what("")
{
	return;
}

AudioManager::InitError::InitError(const std::string error): _what(error)
{
	return;
}

AudioManager::InitError::InitError(const InitError & rhs): _what(rhs._what)
{
	return;
}

AudioManager::InitError::~InitError(void)
{
	return;
}


const char *	AudioManager::InitError::what(void) const noexcept
{
	std::string error;

	error = "Error in AudioManager initialisation";
	if (!_what.empty())
		error += ": " + _what;
	return error.c_str();
}

extern "C" IAudioManager * create(void)
{
	return new AudioManager();
}
