#include "DisplayerGlfw.class.hpp"

void		DisplayerGlfw::_loadImg(void)
{
	unsigned char *	data = new unsigned char [3 * TEXT_SIZE * TEXT_SIZE];
	unsigned char *	line;
	std::string		buf;
	FILE *			fd;
	struct jpeg_decompress_struct cinfo;
	struct jpeg_error_mgr jerr;

	if (!data)
		throw InitError();
	cinfo.err = jpeg_std_error(&jerr);
	jpeg_create_decompress(&cinfo);
	if (0 == (fd = fopen("assets/LEDBlueText.jpg", "rb")))
		throw InitError("Openning text image");
	jpeg_stdio_src (&cinfo, fd);
	jpeg_read_header (&cinfo, TRUE);
	std::cout << "SIZES: " << cinfo.image_width <<
		", " << cinfo.image_height << std::endl;
	if ((cinfo.image_width != TEXT_SIZE) || (cinfo.image_height != TEXT_SIZE))
		throw InitError("Text image size invalid");
	jpeg_start_decompress (&cinfo);
	while (cinfo.output_scanline < cinfo.output_height)
	{
		line = &data[3 * TEXT_SIZE * cinfo.output_scanline + 1];
		jpeg_read_scanlines(&cinfo, &line, 1);
	}
	jpeg_finish_decompress(&cinfo);
	jpeg_destroy_decompress(&cinfo);
	fclose(fd);
	glGenTextures(1, &_textID);
	glBindTexture(GL_TEXTURE_2D, _textID);
	glTexImage2D(GL_TEXTURE_2D, 0,
				 GL_RGB, TEXT_SIZE, TEXT_SIZE,
				 0, GL_BGR, GL_UNSIGNED_BYTE, data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	delete [] data;
}

void		DisplayerGlfw::_putText(const std::string & text, int x, int y)
{
	std::vector<glm::vec2>	vertices;
	std::vector<glm::vec2>	uv;
	int						len = text.length();

	for (int i = 0 ; i < len ; i++)
	{
		glm::vec2 upLeft  = glm::vec2(x + i * LETTER_SIZE, y + LETTER_SIZE);
		glm::vec2 upRight = glm::vec2(x + (i + 1) * LETTER_SIZE, y + LETTER_SIZE);
		glm::vec2 doRight = glm::vec2(x + (i + 1) * LETTER_SIZE, y);
		glm::vec2 doLeft  = glm::vec2(x + i * LETTER_SIZE, y);

		vertices.push_back(doRight);
		vertices.push_back(upRight);
		vertices.push_back(doLeft);

		vertices.push_back(upLeft);
		vertices.push_back(doLeft);
		vertices.push_back(upRight);

		int		letter = text[i] - ' ';
		float	R = static_cast<float>(LETTER_SIZE) / TEXT_SIZE;
		float	X = (letter % 10) * R;
		float	Y = (letter / 10) * R;
		upLeft  = glm::vec2(X    , Y);
		upRight = glm::vec2(X + R, Y);
		doRight = glm::vec2(X + R, Y + R);
		doLeft  = glm::vec2(X    , Y + R);

		uv.push_back(doRight);
		uv.push_back(upRight);
		uv.push_back(doLeft);

		uv.push_back(upLeft);
		uv.push_back(doLeft);
		uv.push_back(upRight);
	}

	GLuint	mesh;
	GLuint	tex;
	glGenBuffers(1, &mesh);
	glBindBuffer(GL_ARRAY_BUFFER, mesh);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec2),
				 &vertices[0], GL_STATIC_DRAW);
	glGenBuffers(1, &tex);
	glBindBuffer(GL_ARRAY_BUFFER, tex);
	glBufferData(GL_ARRAY_BUFFER, uv.size() * sizeof(glm::vec2),
				 &uv[0], GL_STATIC_DRAW);

	glUseProgram(_textShaderID);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, _textID);
	GLuint textID	= glGetUniformLocation(_textID, "textTexture");
	glUniform1i(textID, 0);

	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, mesh);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);

	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, tex);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, 0);

	GLuint screenID = glGetUniformLocation(_textShaderID, "screen");
	glUniform2f(screenID, _win_size.x, _win_size.y);

	glBlendColor(1., 1., 1.0, 0);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);

	glDrawArrays(GL_TRIANGLES, 0, vertices.size());

	glDisable(GL_BLEND);

	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
}

DisplayerGlfw::DisplayerGlfw(void) : _window(NULL), _key(_key_invalid)
{
	ECHO("Displayer Glfw Created");
	return;
}

DisplayerGlfw::~DisplayerGlfw(void)
{
	ECHO("Displayer Glfw Deleted");
	return;
}

const glm::vec3	DisplayerGlfw::_colors[4] =
{
	glm::vec3(1.0f, 0.0f, 0.0f),
	glm::vec3(1.0f, 0.5f, .1f),
	glm::vec3(.1f, 0.8f, .1f),
	glm::vec3(.2f, .2f, .8f)
};

const GLfloat		DisplayerGlfw::_vertArr[] =
{
	-1.0f,-1.0f,-1.0f,
	-1.0f,-1.0f, 1.0f,
	-1.0f, 1.0f, 1.0f,
	1.0f, 1.0f,-1.0f,
	-1.0f,-1.0f,-1.0f,
	-1.0f, 1.0f,-1.0f,
	1.0f,-1.0f, 1.0f,
	-1.0f,-1.0f,-1.0f,
	1.0f,-1.0f,-1.0f,
	1.0f, 1.0f,-1.0f,
	1.0f,-1.0f,-1.0f,
	-1.0f,-1.0f,-1.0f,
	-1.0f,-1.0f,-1.0f,
	-1.0f, 1.0f, 1.0f,
	-1.0f, 1.0f,-1.0f,
	1.0f,-1.0f, 1.0f,
	-1.0f,-1.0f, 1.0f,
	-1.0f,-1.0f,-1.0f,
	-1.0f, 1.0f, 1.0f,
	-1.0f,-1.0f, 1.0f,
	1.0f,-1.0f, 1.0f,
	1.0f, 1.0f, 1.0f,
	1.0f,-1.0f,-1.0f,
	1.0f, 1.0f,-1.0f,
	1.0f,-1.0f,-1.0f,
	1.0f, 1.0f, 1.0f,
	1.0f,-1.0f, 1.0f,
	1.0f, 1.0f, 1.0f,
	1.0f, 1.0f,-1.0f,
	-1.0f, 1.0f,-1.0f,
	1.0f, 1.0f, 1.0f,
	-1.0f, 1.0f,-1.0f,
	-1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f,
	-1.0f, 1.0f, 1.0f,
	1.0f,-1.0f, 1.0f
};

void			DisplayerGlfw::_checkGlError(const std::string & where)
{
	GLint				error;
	std::stringstream	ss;

	if ((error = glGetError()) != GL_NO_ERROR)
	{
		ss << "Gl error in: " << where << ": " << error;
		throw DisplayerGlfw::InitError(ss.str());
	}
}

void			DisplayerGlfw::init(int size_x, int size_y)
{
	if (!glfwInit())
		throw InitError("Glfw Initialisation failed");
	_map_size.x = size_x;
	_map_size.y = size_y;
	_win_size.x = (size_x * SQUARE_SIZE * 10);
	_win_size.y = (size_y * SQUARE_SIZE * 10);

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	_window = glfwCreateWindow(_win_size.x, _win_size.y,
							   "Nibbler Glfw - OpenGL", NULL, NULL);
	if (!_window)
	{
		glfwTerminate();
		throw InitError("Window creation failed");
	}
	glfwSetWindowUserPointer(_window, this);
	glfwSetKeyCallback(_window, _key_callback);
	glfwSetWindowCloseCallback(_window, _win_close_callback);
	glfwMakeContextCurrent(_window);
	glfwSwapInterval(1);

	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);
	glfwGetFramebufferSize(_window, &_glViewSize.x, &_glViewSize.y);
	_winRatio = _glViewSize.x / static_cast<float>(_glViewSize.y);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	float	range = fmax(_map_size.x, _map_size.y) - MAP_X_MIN;
	range *= range > 0.f ? 3.0f : 1.0f;
	_projection = glm::perspective(44.7f, _winRatio, 50.f, 1000.0f);
	float	ratio = _winRatio;
	ratio = _winRatio < 0.9 ? std::abs(1.f - ratio * 0.9) : ratio;
	float recul;
	if (_winRatio <= 1)
	{
		recul = size_y - 20;
		recul = (recul / 30) * 120 + 90;
	}
	else
	{
		recul = size_x - 20;
		recul = 80 + (2.5f - _winRatio) / 2.5f * 160;
	}
	_view       = glm::lookAt(glm::vec3(10.0f, -100.2f, recul),
							  glm::vec3(0.f, 0.f, 0.0f),
							  glm::vec3(0.f, 1.f, 0.f));

	glGenVertexArrays(1, &_test);
	_checkGlError("Vertext Array generation");
	glBindVertexArray(_test);
	_checkGlError("Vertext Array binding");
	_shaderID = _loadShader("assets/vertexSnake.shad", "assets/fragmentSnake.shad");
	_checkGlError("Shader program loading");
	_loadMesh();
	_textShaderID = _loadShader("assets/textShader.v", "assets/textShader.f");
	_loadImg();
}

void			DisplayerGlfw::_loadMesh(void)
{
	glGenBuffers(1, &_vBuffer);
	_checkGlError("Buffer Generation");
	glBindBuffer(GL_ARRAY_BUFFER, _vBuffer);
	_checkGlError("Buffer binding");
	glBufferData(GL_ARRAY_BUFFER, sizeof(_vertArr),
				 _vertArr, GL_STATIC_DRAW);
	_checkGlError("Buffer Setting data");
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	_checkGlError("Vertex pointer attribution");
}

void			DisplayerGlfw::close(void)
{
	glfwDestroyWindow(_window);
	glfwTerminate();
	ECHO("Closed Displayer GLFW");
	return;
}

void			DisplayerGlfw::put_score(const int score)
{
	std::stringstream ss;

	ss << "Score: " << score;
	_putText(ss.str(), 20, 20);
}

void			DisplayerGlfw::put_framerate(const int framerate)
{
	std::stringstream ss;

	ss  << framerate;
	_putText(ss.str(), 20, _win_size.y - SQUARE_SIZE * 10 - 10);
}

void			DisplayerGlfw::put_time(int sec)
{
	std::stringstream ss;

	ss  << "Time: " << sec;
	_putText(ss.str(), _win_size.x - SQUARE_SIZE * 10 * ss.str().length() - SQUARE_SIZE * 10 * 2 - 10,
			_win_size.y - SQUARE_SIZE * 10 - 10);

}

void			DisplayerGlfw::put_entity(float x, float y, e_type type)
{
	glm::mat4	trans	= glm::translate(
		glm::vec3(
			-_map_size.x * SQUARE_SIZE / 2 + x * SQUARE_SIZE + 0.5 * SQUARE_SIZE,
			_map_size.y * SQUARE_SIZE / 2 + -y * SQUARE_SIZE - 0.5 * SQUARE_SIZE,
			0.f)
		);
	glm::mat4	MVP		= _projection * _view * trans;
	glm::vec3	color	= _colors[type];

	glUseProgram(_shaderID);
	glEnableVertexAttribArray(0);
	GLuint colorID = glGetUniformLocation(_shaderID, "color");
	glUniform3f(colorID, color.x, color.y, color.z);
	GLuint MatrixID = glGetUniformLocation(_shaderID, "MVP");
	glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
	glDrawArrays(GL_TRIANGLES, 0, 12 * 3);
	glDisableVertexAttribArray(0);
}

void			DisplayerGlfw::_putWalls(void)
{
	int		i;

	for (i = 0 ; i < _map_size.x ; i++)
	{
		put_entity(i, -1.f, _type_snake);
		put_entity(i, _map_size.y, _type_snake);
	}
	for (i = 0 ; i < _map_size.y ; i++)
	{
		put_entity(-1.f, i, _type_snake);
		put_entity(_map_size.x, i, _type_snake);
	}
}

e_key			DisplayerGlfw::get_input(void) const
{
	glfwPollEvents();
	return _key;
}

void			DisplayerGlfw::_setCurrentKey(const e_key key)
{
	_key = key;
}

Keys	DisplayerGlfw::_keys =
{
	{GLFW_KEY_ESCAPE, _key_exit},
	{GLFW_KEY_RIGHT, _key_right},
	{GLFW_KEY_LEFT, _key_left},
	{GLFW_KEY_UP, _key_up},
	{GLFW_KEY_DOWN, _key_down},
	{GLFW_KEY_1, _key_lib1},
	{GLFW_KEY_2, _key_lib2},
	{GLFW_KEY_3, _key_lib3},
	{GLFW_KEY_SPACE, _key_pause}
};

void			DisplayerGlfw::_win_close_callback(GLFWwindow *win)
{
	DisplayerGlfw *     displayer;

	displayer = static_cast<DisplayerGlfw *>(glfwGetWindowUserPointer(win));
	if (!displayer)
		return;
	displayer->_setCurrentKey(_key_exit);
}

void			DisplayerGlfw::_key_callback(GLFWwindow* window, int key,
											int scancode, int action, int mods)
{
	(void)mods;
	(void)scancode;
	DisplayerGlfw *		displayer;

	displayer = static_cast<DisplayerGlfw *>(glfwGetWindowUserPointer(window));
 	if (!displayer || action != GLFW_PRESS)
		return;
	displayer->_setCurrentKey(_key_invalid);
	if (displayer->_keys.find(key) != displayer->_keys.end())
	{
		if (displayer->_key != displayer->_keys[key])
			displayer->_setCurrentKey(displayer->_keys[key]);
	}
}

void			DisplayerGlfw::clear_window(void)
{
	glfwGetFramebufferSize(_window, &_glViewSize.x, &_glViewSize.y);
	_winRatio = _glViewSize.x / static_cast<float>(_glViewSize.y);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	_key = _key_invalid;
	_putWalls();
	return;
}

void			DisplayerGlfw::draw(void)
{
	glfwSwapBuffers(_window);
}

const std::string		DisplayerGlfw::_getFileContent(const std::string & path)
{
	std::ifstream	ifs(path);
	std::string		ret;
	std::string		line;

	if (!ifs.is_open())
		throw DisplayerGlfw::InitError();
	while (std::getline(ifs, line))
		ret += "\n" + line;
	ifs.close();
	return ret;
}

void		DisplayerGlfw::_checkShaderError(GLuint shader, GLuint flag,
											 bool isProgram, const std::string info)
{
	GLint ok;

	GLchar error[4096];
	if (isProgram)
		glGetProgramiv(shader, flag, &ok);
	else
		glGetShaderiv(shader, flag, &ok);
	if (ok == GL_FALSE)
	{
		if (isProgram)
			glGetProgramInfoLog(shader, sizeof(error), NULL, error);
		else
			glGetShaderInfoLog(shader, sizeof(error), NULL, error);
		throw DisplayerGlfw::InitError(info + ": " + error);
	}
}

GLuint			DisplayerGlfw::_loadShader(const std::string & vShaderPath,
										   const std::string & fShaderPath)
{
	GLuint	vShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint	fShaderID = glCreateShader(GL_FRAGMENT_SHADER);
	if (!vShaderID || !fShaderID)
		throw InitError("Invalid shader ID");

	std::string fileContent = _getFileContent(vShaderPath);
	const char * str = fileContent.c_str();
	const GLint	strLen = fileContent.length();
	glShaderSource(vShaderID, 1, &str, &strLen);
	glCompileShader(vShaderID);
	_checkShaderError(vShaderID, GL_COMPILE_STATUS, false, "Vertex Shader Compilation");

	fileContent = _getFileContent(fShaderPath);
	const char * str1 = fileContent.c_str();
	const GLint	strLen1 = fileContent.length();
	glShaderSource(fShaderID, 1, &str1, &strLen1);
	glCompileShader(fShaderID);
	_checkShaderError(fShaderID, GL_COMPILE_STATUS, false, "Fragment Shader Compilation");

	GLuint programID = glCreateProgram();
	if (!programID)
		throw InitError("Invalid program ID");
    glAttachShader(programID, vShaderID);
    glAttachShader(programID, fShaderID);
    glLinkProgram(programID);
	_checkShaderError(programID, GL_LINK_STATUS, true, "Program linking");
	glValidateProgram(programID);
	_checkShaderError(programID, GL_VALIDATE_STATUS, true, "Program Validation");
	glDeleteShader(vShaderID); 
	glDeleteShader(fShaderID); 
	return programID;
}

DisplayerGlfw::InitError::InitError(void): _what("")
{
	return;
}

DisplayerGlfw::InitError::InitError(const std::string error): _what(error)
{
	return;
}

DisplayerGlfw::InitError::InitError(const InitError & rhs): _what(rhs._what)
{
	return;
}

DisplayerGlfw::InitError::~InitError(void)
{
	return;
}


const char *	DisplayerGlfw::InitError::what(void) const noexcept
{
	std::string error;

	error = "Error in glfw displayer initialisation";
	if (!_what.empty())
		error += ": " + _what;
	return error.c_str();
}

extern "C" IDisplayer * create(void)
{
	return new DisplayerGlfw();
}
