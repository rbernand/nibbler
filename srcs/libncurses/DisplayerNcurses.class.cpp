#include <DisplayerNcurses.class.hpp>

std::map<int, e_key> DisplayerNcurses::_key_map = DisplayerNcurses::create_map();

DisplayerNcurses::DisplayerNcurses ( void )
{
	std::cout << "DisplayerNcurses created" << std::endl;
	return ;
}

DisplayerNcurses::~DisplayerNcurses (void)
{
	std::cout << "DisplayerNcurses destructed" << std::endl;
	return ;
}

void			DisplayerNcurses::init(int size_x, int size_y)
{
	_size_x = size_x;
	_size_y = size_y;
	initscr();
	noecho();
	curs_set(0);
	keypad(stdscr, TRUE);
	nodelay(stdscr, TRUE);
	start_color();
	init_pair(_WHITE, COLOR_WHITE, COLOR_BLACK);
	init_pair(_RED, COLOR_RED, COLOR_BLACK);
	init_pair(_GREEN, COLOR_GREEN, COLOR_BLACK);
	init_pair(_BLUE, COLOR_CYAN, COLOR_BLACK);
	init_pair(_RV, COLOR_BLACK, COLOR_WHITE);
	_main_win = newwin(size_y + 2, size_x * 2 + 3, OFFSET_Y, OFFSET_X);
	_gui_win = newwin(size_y + 2, 25, OFFSET_Y, OFFSET_X + size_x * 2 + 3);
}

void			DisplayerNcurses::close( void )
{
	endwin();
}

void	DisplayerNcurses::put_score(const int score)
{
	mvwprintw(_gui_win, 1, 1, "Score : %d", score);
}

void	DisplayerNcurses::put_framerate(const int framerate)
{
	mvwprintw(_gui_win, 2, 1, "FPS : %d", framerate);
}

void			DisplayerNcurses::_putShape(int y, int x, const char * shape, chtype ch)
{
	wattron(_main_win, ch);
	mvwaddch(_main_win, y, x * 2, shape[0]);
	mvwaddch(_main_win, y, x * 2 + 1, shape[1]);
	wattroff(_main_win, ch);

}

void			DisplayerNcurses::put_entity(float fx, float fy, e_type type)
{
	int				x = static_cast<int>(fx) + 1;
	int				y = static_cast<int>(fy) + 1;

	if (type == _type_snake)
		_putShape(y, x, "()", COLOR_PAIR(_RED));
	else if (type == _type_snake_head)
		_putShape(y, x, "88", COLOR_PAIR(_BLUE));
	else if (type == _type_food)
		_putShape(y, x, "{}", COLOR_PAIR(_WHITE));
	else if (type == _type_special_food)
		_putShape(y, x, "[]", COLOR_PAIR(_GREEN));
}

void			DisplayerNcurses::put_time ( int sec)
{
	mvwprintw(_gui_win, 3, 1, "Time : %d", sec);
}

e_key			DisplayerNcurses::get_input ( void ) const
{
	int				key = getch();
	static std::map<int, e_key>::iterator			notFound = _key_map.end();

	if (DisplayerNcurses::_key_map.find(key) != notFound)
		return (DisplayerNcurses::_key_map[key]);
	else
		return (_key_invalid);
}

void			DisplayerNcurses::clear_window ( void )
{
	wclear(_main_win);
	wclear(_gui_win);
	box(_main_win, '|', '-');
	box(_gui_win, '|', '*');
}

void			DisplayerNcurses::draw ( void )
{
	wrefresh(_main_win);
	wrefresh(_gui_win);
}

std::map<int, e_key>		DisplayerNcurses::create_map ( void )
{
	std::map<int, e_key>			m;

	m[KEY_UP] = _key_up;
	m[KEY_DOWN] = _key_down;
	m[KEY_LEFT] = _key_left;
	m[KEY_RIGHT] = _key_right;
	m[27] = _key_exit;
	m['1'] = _key_lib1;
	m['2'] = _key_lib2;
	m['3'] = _key_lib3;
	m[' '] = _key_pause;
	return (m);
}

extern "C" IDisplayer *			create( void )
{
	return ( new DisplayerNcurses() );
}
