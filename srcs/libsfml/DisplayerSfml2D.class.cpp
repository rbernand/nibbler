#include "DisplayerSfml2D.class.hpp"

DisplayerSfml2D::DisplayerSfml2D(void)
{
	ECHO("Displayer sfml 2D created");
	return;
}

DisplayerSfml2D::~DisplayerSfml2D(void)
{
	ECHO("Displayer sfml 2D deleted");
	return;
}

void	DisplayerSfml2D::init(int size_x, int size_y)
{
	std::string	assetsFolder(ASSETS_FOLDER);
	std::string	fontName(FONT_NAME);

	_map_size.x = size_x;
	_map_size.y = size_y;
	_win_size.x = (_map_size.x * SQUARE_SIZE) + BOUND_SIZE * 2;
	_win_size.y = (_map_size.y * SQUARE_SIZE) + BOUND_SIZE * 2 + GUI_HEIGHT;
	_win = new sf::RenderWindow(
		sf::VideoMode(_win_size.x, _win_size.y), "Nibbler 2D SFML");
	if (!_win)
		throw InitError();
	if (!_guiFont.loadFromFile(assetsFolder + "/" + fontName))
		throw InitError();
}

void	DisplayerSfml2D::close(void)
{
	this->_win->close();
	delete this->_win;
}

void	DisplayerSfml2D::put_entity(float x, float y, e_type type)
{
	if (type == _type_snake)
		_putSnakeElem(x, y, sf::Color(100, 250, 50));
	else if (type == _type_snake_head)
		_putSnakeElem(x, y, sf::Color(200, 250, 50));
	else if (type == _type_food)
		_putFood(x, y);
	else if (type == _type_special_food)
		_putSpecialFood(x, y);
}

void	DisplayerSfml2D::_putFood(float x, float y)
{
	sf::CircleShape food(SQUARE_SIZE / 2);

	food.setFillColor(sf::Color(180, 0, 60));
	food.setPosition(BOUND_SIZE + (x * SQUARE_SIZE),
						  BOUND_SIZE + GUI_HEIGHT + (y * SQUARE_SIZE));
	this->_win->draw(food);
}

void	DisplayerSfml2D::_putSpecialFood(float x, float y)
{
	sf::CircleShape food(SQUARE_SIZE / 2);

	food.setFillColor(sf::Color(255, 255, 255));
	food.setPosition(BOUND_SIZE + (x * SQUARE_SIZE),
						  BOUND_SIZE + GUI_HEIGHT + (y * SQUARE_SIZE));
	this->_win->draw(food);
}

void	DisplayerSfml2D::put_score(const int score)
{
	int					charHeight;
	float               textYpos;
	std::stringstream   ss;
	sf::Text			text;
	std::string			str;

	charHeight = (BOUND_SIZE + GUI_HEIGHT) / 2;
	textYpos = (BOUND_SIZE + GUI_HEIGHT) / 4;
	text.setFont(_guiFont);
	text.setCharacterSize(charHeight);
	text.setColor(sf::Color::White);
	ss << score;
	str = "Score: " + ss.str();
	text.setString(str.c_str());
	text.setPosition(_win_size.x - BOUND_SIZE - (str.length() * (charHeight / 1.9)),
					 textYpos);	
	_win->draw(text);
}

void	DisplayerSfml2D::put_framerate(const int framerate)
{
	int					charHeight;
	float               textYpos;
	std::stringstream   ss;
	sf::Text			text;
	std::string			str;

	charHeight = (BOUND_SIZE + GUI_HEIGHT) / 2;
	textYpos = (BOUND_SIZE + GUI_HEIGHT) / 4;
	text.setFont(_guiFont);
	text.setCharacterSize(charHeight);
	text.setColor(sf::Color::White);
	text.setPosition(BOUND_SIZE, textYpos);	
	ss << framerate;
	str = ss.str() + " fps";
	text.setString(str.c_str());
	_win->draw(text);
}

void	DisplayerSfml2D::_putSnakeElem(float x, float y, const sf::Color & color)
{
	sf::RectangleShape rectangle(sf::Vector2f(SQUARE_SIZE, SQUARE_SIZE));

	rectangle.setFillColor(color);
	rectangle.setPosition(BOUND_SIZE + (x * SQUARE_SIZE),
						  BOUND_SIZE + GUI_HEIGHT + (y * SQUARE_SIZE));
	this->_win->draw(rectangle);
}

e_key	DisplayerSfml2D::get_input(void) const
{
	sf::Event	event;
	e_key		ret;
	static std::map<int, e_key> keys = {{sf::Keyboard::Space, _key_pause},
									   {sf::Keyboard::Num1, _key_lib1},
									   {sf::Keyboard::Num2, _key_lib2},
									   {sf::Keyboard::Num3, _key_lib3}};

	ret = this->_getMoves();
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
		ret = _key_exit;
	while (_win->pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
			ret = _key_exit;
		else if (event.type == sf::Event::KeyPressed)
		{
			if (keys.find(event.key.code) != keys.end())
				ret = keys[event.key.code];
		}
	}
	return ret;
}

void		DisplayerSfml2D::put_time(int sec)
{
	int					charHeight;
	float               textYpos;
	std::stringstream   ss;
	sf::Text			text;
	std::string			str;

	charHeight = (BOUND_SIZE + GUI_HEIGHT) / 2;
	textYpos = (BOUND_SIZE + GUI_HEIGHT) / 4;
	text.setFont(_guiFont);
	text.setCharacterSize(charHeight);
	text.setColor(sf::Color::White);
	ss << sec;
	str = ss.str();
	str = "Time: " + str;
	text.setPosition(_win_size.x / 2 + BOUND_SIZE - 
					 (str.length() * (charHeight / 1.8)) / 2,
					 textYpos);
	text.setString(str.c_str());
	_win->draw(text);
}

e_key	DisplayerSfml2D::_getMoves(void) const
{
	e_key		ret;
	e_key		moves[4];
	int			i;
	int			movesCount = 0;

	for (i = 0 ; i < 4 ; i++)
		moves[i] = _key_invalid;
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
		moves[0] = _key_left;
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
		moves[1] = _key_right;
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
		moves[2] = _key_up;
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
		moves[3] = _key_down;
	for (i = 0 ; i < 4 ; i++)
	{
		if (moves[i] != _key_invalid)
		{
			movesCount++;
			ret = moves[i];
		}
	}
	if (movesCount != 1)
		ret = _key_invalid;
	return ret;
}

void	DisplayerSfml2D::clear_window(void)
{
	sf::RectangleShape rectangle(sf::Vector2f(_map_size.x * SQUARE_SIZE,
											  _map_size.y * SQUARE_SIZE));

	this->_win->clear(sf::Color::Black);
	rectangle.setFillColor(sf::Color(40, 40, 40));
	rectangle.setPosition(BOUND_SIZE, BOUND_SIZE + GUI_HEIGHT);
	this->_win->draw(rectangle);
}

void	DisplayerSfml2D::draw(void)
{
	this->_win->display();
}

const char *	DisplayerSfml2D::InitError::what(void) const noexcept
{
	return "Error in sfml displayer initialisation";
}

extern "C" IDisplayer * create(void)
{
	return new DisplayerSfml2D();
}
